#!/bin/bash

#clean up
echo ""> scanmemIN
echo ""> scanmemOUT

echo "running scanmem"
tail -f scanmemIN | scanmem &>> scanmemOUT &
scanmemPID=$!
./fish.py
echo "---------------------"
kill scanmemPID
echo "Killed scanmem"
