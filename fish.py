#!/usr/bin/python3

from __future__ import print_function

import sys
import readline
from os import environ
import commands




def readCMDs():
  cmdFile = open('./commands.py')
  foundCMDs = []
  cmd = ""
  
  for line in cmdFile:
    if 'def ' in line:
      cmd = line.split(" ")[1].split("(")[0].strip()
      foundCMDs.append(cmd)
  return foundCMDs




class badAssCompleter(object):  # Custom completer
  def __init__(self, options):
    self.options = sorted(options)

  def complete(self, text, state):
    if state == 0:  # on first trigger, build possible matches
      if not text:
        self.matches = self.options[:]
      else:
        self.matches = [s for s in self.options if s and s.startswith(text)]

    # return match indexed by state
    try:
      return self.matches[state]
    except IndexError:
      return None

  def display_matches(self, substitution, matches, longest_match_length):
    line_buffer = readline.get_line_buffer()
    columns = environ.get("COLUMNS", 80)

    print()

    buffer = ""


    for match in matches:
      match = match + "\t"
      if len(buffer + match) > columns:
        print(buffer)
        buffer = ""
      buffer += match

    if buffer:
      print(buffer)

    print("> ", end="")
    print(line_buffer, end="")
    sys.stdout.flush()




CMDs = readCMDs()

CMDs = [x.split(' ')[0] for x in CMDs]

completer = badAssCompleter(list(set(CMDs)))
readline.set_completer_delims(' \t\n;')
readline.set_completer(completer.complete)
readline.parse_and_bind('tab: complete')
readline.set_completion_display_matches_hook(completer.display_matches)




#stuff is set up, Main loop
while True:
  cmd_raw = input("><> ")
  cmd = cmd_raw.split(" ")[0]
  args = []
  if len(cmd_raw.split(" ")) > 1:
    args = cmd_raw.split(" ")[1:]
  if cmd in CMDs:
    argsPart = "'" + "','".join(args) + "'"
    evalCommand = "commands." + cmd + "(" + argsPart + ")"
    eval(evalCommand)
  else:
    for line in commands.functions.runScanmemCMD(cmd_raw):
      print(line.strip())
      
      
    
