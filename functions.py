import time, io
global endPosition
endPosition = 0
OUT = open("./scanmemOUT")
IN = open("./scanmemIN", 'a')


def runScanmemCMD_raw(cmd):
  #open stdin file and write data
  IN = open("./scanmemIN", 'a')
  IN.write(cmd + "\n")
  IN.close()
  

def runScanmemCMD(cmd):
  #open stdin file and write data
  IN = open("./scanmemIN", 'a')
  IN.write(cmd + "\n")
  IN.close()
  
  #read output
  global endPosition
  OUT.seek(0, io.SEEK_END)


  while True:
    time.sleep(.01)
    while True:
      line = OUT.readline()
      yield line
      if ">" in line:
        return line.split(">")[0]
      newPos = OUT.tell()
      if newPos == endPosition:
        break
      else:
        endPosition = newPos


def betterList():
  runScanmemCMD_raw('update')
  time.sleep(0.01)
  data = []
  for line in runScanmemCMD('list'):
    if line.startswith("["):
      data.append(line.split(",")[3].strip())
    
  return data
